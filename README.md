<h1 align="center">
  React Starter Typescript
</h1>
<p align="center">Work in Progress. Just I wanted to understand how works react and typescript behind scenes.</p>
<p align="center">
  <a href="https://github.com/xorb/demo"><img src="https://img.shields.io/travis/com/xorb/node-demo-app.svg?style=flat-square" alt="build status"></a>
  <a href="https://github.com/xorb/demo"><img src="https://img.shields.io/coveralls/github/xorb/node-demo-app.svg?style=flat-square" alt="code coverage"></a>
  <a href="https://github.com/xorb/demo"><img src="https://img.shields.io/github/stars/xorb/node-demo-app.svg?style=flat-square" alt="code coverage"></a>
  <a href="https://github.com/xorb/demo"><img src="https://img.shields.io/github/last-commit/xorb/node-demo-app.svg?style=flat-square" alt="last commit"></a>
  <a href="https://github.com/xorb/node-demo-app/blob/master/LICENCE"><img src="https://img.shields.io/github/license/xorb/node-demo-app.svg?style=flat-square" alt="license"></a>  
</p>

## Motivation
I just wanted to create an amazing demo for my future projects. So this is a base structure. Hope you can find useful and enjoy it.

## Folder structure

```
├── config
│   ├── webpack.config.common.ts
│   ├── webpack.config.dev.ts
│   └── webpack.config.prod.ts
├── LICENSE
├── package.json
├── README.md
├── src
│   ├── App.tsx
│   ├── index.css
│   ├── index.html
│   └── index.tsx
├── tsconfig.json
└── yarn.lock
```

