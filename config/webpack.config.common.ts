import { Configuration } from 'webpack'
import { join } from 'path'
import HtmlWebpackPlugin from 'html-webpack-plugin'
const projectDirectory = join(__dirname, '..')

const config: Configuration = {
  entry: {
    app: join(projectDirectory, 'src/index.tsx')
  },
  output: {
    path: join(projectDirectory, 'dist'),
    filename: 'app.bundle.js'
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', 'jsx']
  },
  module: {
    rules: [
      {test: /\.tsx?$/, use: 'ts-loader', exclude: /node_modules/},
      {test: /\.css$/, use: ['style-loader', 'css-loader'], exclude: /node_modules/}
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: join(projectDirectory, 'src/index.html'),
      inject: 'body',
      react: `react@${process.env.npm_package_dependencies_react}`,
      reactDOM: `react-dom@${process.env.npm_package_dependencies_react_dom}`
    })
  ]
}

export default config