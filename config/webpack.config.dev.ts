import * as webpackDevServer from 'webpack-dev-server'
import {Configuration} from 'webpack'
import merge from 'webpack-merge'
import webpackCommonConfig from './webpack.config.common'
import ip from 'ip'
const config: Configuration = merge(webpackCommonConfig, {
  mode: 'development',
  devtool: 'source-map',
  devServer: {
    contentBase: './dist',
    open: true,
    host: ip.address(),
    port: 8020
  }
})

export default config